import Vue from "vue";
import App from "./App.vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import VueRouter from "vue-router";
import Home from "./components/Home/home.vue";
import Signup from "./components/Signup/signup.vue";
import Login from "./components/Login/login.vue";
import Profile from "./components/Profile/profile.vue";
import elearning from "./components/E-Learning/e-learning.vue";
import videos from "./components/Videos/videos.vue";
import HowToParticipate from "./components/HowToParticipate/howToParticipate.vue";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faUserSecret } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import axios from "axios";
import VueAxios from "vue-axios";

library.add(faUserSecret);

Vue.component("font-awesome-icon", FontAwesomeIcon);

// Install BootstrapVue
Vue.use(BootstrapVue);
Vue.use(VueAxios, axios);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);
Vue.use(VueRouter);
Vue.config.productionTip = false;

const routes = [
  { path: "/", component: Home },
  { path: "/signUp", component: Signup },
  { path: "/login", component: Login },
  { path: "/how-to-participate", component: HowToParticipate },
  { path: "/profile", component: Profile },
  { path: "/e-learning", component: elearning },
  { path: "/videos", component: videos },

];
const router = new VueRouter({
  routes,
  mode: "history"
});
new Vue({
  render: h => h(App),
  router
}).$mount("#app");
